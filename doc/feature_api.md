## flexSearch4cj 库

### 介绍
FlexSearch 是一个快速、零依赖的全文搜索库。 在原始搜索速度方面，FlexSearch 优于每一个搜索库， 并提供灵活的搜索功能，如多字段搜索，语音转换或部分匹配。根据使用的选项，它还提供最高内存效率的索引。 FlexSearch 引入了一种新的评分算法，称为“上下文索引”，基于预先评分的词典字典体系结构，与其他库相比，实际执行的查询速度有大幅度提高。 FlexSearch 还为您提供非阻塞异步处理模型，以通过专用平衡线程并行地对索引执行任何更新或查询。

### 1 索引选项

前置条件：NA
场景：
    用于Index的选项
    支持配置多种初始化选项，如 Preset 预配置搜索选项等
约束：cangjie 实现的 options 选项逻辑是Json 格式的值在进行预配置项的判断及设置
性能： NA
可靠性： NA

#### 1.1 IndexOption
```cangjie

//可选编码模式 CharsetOptions.Simple.encode | CharsetOptions.Balance.encode | CharsetOptions.Advanced.encode | CharsetOptions.Extra.encode | CharsetOptions.CjkDefault.encode | CharsetOptions.CyrDefault.encode | CharsetOptions.AraDefault.encode 支持自定义编码函数类型如下
public type EncodeFunc = (String) -> ArrayList<String>

//该函数具有以下签名：Function（words[]， term， index） => Float。它有 3 个参数，您可以在其中获得所有单词的数组、当前术语和术语放置在单词数组中的当前索引。您可以应用自己的计算，例如，一个术语的出现并返回此因子（<1 表示相关性降低，>1 表示相关性增加）。注意：此功能目前仅通过使用分词器“strict”而受到限制。
public type BoostFunc = (ArrayList<String>, String, Int64) -> Float64

/**
 * 索引选项
 *   Context 上下文预设
 *   Charset 分词选项
 *   Language 分词流程 
 */
public open class IndexOptions {
    /**
     * 设置缓存大小
     **/
    public mut prop cache: CacheClass
    /**
     * 指定添加数据的存储顺序，false(从左到右)，true(从右到左)
     * 默认 false
     */
    public mut prop rtl: Bool
    /**
     * 分词方法
     * 默认 CharsetOptions.Default.encode 
     **/
    public mut prop encode: EncodeFunc
    /**
     * 分词模式
     * 默认 strict，添加所有数据索引，可选项分词模式 "strict" | "forward" | "reverse" | "full"，"forward" 是正向递增数据索引，"reverse" 反向递增数据索引，"full" 添加数据索引所有可能的组合
     **/
    public mut prop tokenize: String
    /**
     * 数据的快速更新
     * 默认 true
     **/
    public mut prop fastupdate: Bool
    /**
     * term分词的最小长度
     * 默认 1
     **/
    public mut prop minlength: Int64
    /**
     * 启用后,会对数据索引使用内存优化的堆栈流
     * 默认 true
     **/
    public mut prop optimize: Bool
    /**
     * 设置添加数据内部的存储空间
     * 默认 9
     **/
    public mut prop resolution: Int64
    /**
     * 深度（上下文）数据的存储距离
     * 默认 1
     **/
    public mut prop contextDepth: Int64
    /**
     * 上下文评分数据的存储距离
     * 默认 1
     **/
    public mut prop contextResolution: Int64
    /**
     * 是否使用数据上下文双向数据的存储
     * 默认 true
     **/
    public mut prop contextBidirectional: Bool
    /**
     * 将内容索引时使用的自定义增强函数
     * 默认 None
     **/
    public mut prop boost: ?BoostFunc
    /**
     * 自定义匹配器，推荐使用 LangAT.matcher | LangDE.matcher | LangEN.matcher | LangUS.matcher
     * 默认 None
     **/
    public mut prop matcher: ?HashMap<String, String>
    /**
     * 词干加工器，推荐使用 LangAT.stemmer | LangDE.stemmer | LangEN.stemmer | LangUS.stemmer
     * 默认 None
     **/
    public mut prop stemmer: ?HashMap<String, String>
    /**
     * 过滤器，推荐使用 LangAT.filter | LangDE.filter | LangEN.filter | LangUS.filter
     * 默认 None
     **/
    public mut prop filter: ?ArrayList<String>

}
public class CacheClass{
    /**
     * limit 小于等于0，不启动缓存
     * 
     **/
    public init(limit: Int64)
}
```
#### 1.2 CharsetOptions
定义 分词方法(encode) 文字方向(rtl) 解析模式(tokenize)
支持阿拉伯语系、中日韩语系、西里尔语系、拉丁语系四种语系

```cangjie

/**
 * 语言选项
 */
public open class CharsetOptions {

    /**
     * 构造 CharsetOptions 
     * 参数 encode - CharsetEncode, 分词方法
     * 参数 rtl - CharsetRtl , 指定文字阅读方向
     * 参数 tokenize - CharsetTokenize，分词模式
     */
    public init(encode: CharsetEncode, rtl: CharsetRtl, tokenize: CharsetTokenize)
    /**
     * 预设区分大小写编码
     */
    public static let Default：CharsetOptions
    /**
     * 预设区分大小写编码，字符集规范化
     */
    public static let Simple：CharsetOptions
    /**
     * 预设区分大小写编码，字符集规范化，文字转换
     */
    public static let Balance：CharsetOptions
    /**
     * 预设区分大小写编码，字符集规范化，文字转换，pairs语音规范化
     */
    public static let Advanced：CharsetOptions
    /**
     * 预设区分大小写编码，字符集规范化，文字转换，pairs语音规范化，Soundex转换
     */
    public static let Extra：CharsetOptions
    /**
     * 阿拉伯字符集处理
     */
    public static let CjkDefault：CharsetOptions
    /**
     * 西里尔字符集处理
     */
    public static let CyrDefault：CharsetOptions
    /**
     * 阿拉伯字符集处理
     */
    public static let AraDefault：CharsetOptions

    /**
     * 指定添加数据的存储顺序，false(从左到右)，true(从右到左)
     */
    public mut prop rtl: CharsetRtl
    /**
     * 分词方法
     **/
    public mut prop encode: CharsetEncode
    /**
     * 分词模式
     **/
    public mut prop tokenize: CharsetTokenize
}
//可选编码模式
public type CharsetEncode = (String) -> ArrayList<String>
//指定文字方向
public type CharsetRtl = Bool
//分词模式
public type CharsetTokenize = String
```

#### 1.3 LanguageOptions
定义了 matcher（自定义替换） stemmer（词缀替换） filter（停用词过滤）逻辑
支持四种拉丁语系语言 at de en us
```cangjie

/**
 * 用于自定义LanguageOptions
 */
public interface LanguageOptions {
    prop stemmer: HashMap<String, String>
    prop filter: ArrayList<String>
    prop matcher: HashMap<String, String>
}
public class LangAT <: LanguageOptions {
    public prop stemmer: HashMap<String, String>
    public prop filter: ArrayList<String>
    public prop matcher: HashMap<String, String>
}
public class LangDE <: LanguageOptions {
    public prop stemmer: HashMap<String, String>
    public prop filter: ArrayList<String>
    public prop matcher: HashMap<String, String>
}
public class LangEN <: LanguageOptions {
    public prop stemmer: HashMap<String, String>
    public prop filter: ArrayList<String>
    public prop matcher: HashMap<String, String>
}
public class LangUS <: LanguageOptions {
    public prop stemmer: HashMap<String, String>
    public prop filter: ArrayList<String>
    public prop matcher: HashMap<String, String>
}
```

### 2 Index 单字段索引搜索

前置条件：NA
场景：
支持 支持 Index 搜索索引
约束：NA
性能： NA
可靠性： NA

#### 2.1 Index
    index单字段检索
```cangjie
public class Index {
    
    /**
     * 构造 Index ,推荐使用 Preset 指定的预定义策略
     * 参数 options - IndexOptions，索引选项用于查询逻辑
     */
    public init(options: IndexOptions)

    /**
     * 追加数据
     * 
     * 参数 id - String,索引ID
     * 参数 content - String,索引内容
     * 
     * 返回值 Index - 返回Index对象
     *
     * 异常 IllegalArgumentException：如果content为null(\0)字符，抛出异常
     */
    public func append(id: String, content: String): Index

    /**
     * 添加数据
     * 
     * 参数 id - String,索引ID
     * 参数 content - String,索引内容
     * 
     * 返回值 Index - 返回Index对象
     *
     * 异常 IllegalArgumentException：如果content为null(\0)字符，抛出异常
     */
    public func add(id: String, content: String): Index

    /**
     * 根据指定条件进行查询，不指定时使用默认值
     * 
     * 参数 querys - String，默认值为空
     * 参数 options - IndexSearchOptions，默认值为空构造
     * 
     * 返回值 ArrayList<String> - 返回搜索内容集合
     */
    public func search(querys!: String = "", options!: IndexSearchOptions = IndexSearchOptions()): ArrayList<String>

    /**
     * 判断数据是否存在
     * 
     * 参数 id - String,索引ID
     * 
     * 返回值 Bool - 返回值，如果是true,数据存在，如果是false,数据不存在
     */
    public func contain(id: String)：Bool

    /**
     * 根据ID更新数据
     * 
     * 参数 id - String,索引ID
     * 参数 content - String,索引内容
     * 
     * 返回值 Index - 返回Index对象
     */
    public func update(id: String, content: String): Index 

    /**
     * 根据ID删除数据
     * 
     * 参数 id - String,索引ID
     * 
     * 返回值 Index - 返回Index对象
     */
    public func remove(id: String): Index 

    /**
     * 异步追加数据
     * 
     * 参数 id - String,索引ID
     * 参数 content - String,索引内容
     * 参数 callback - ?callbackfunc,可参考具体回调函数类型
     * 返回值 Future<Index> 
     *
     * 异常 IllegalArgumentException：如果content为null(\0)字符，抛出异常
     */
    public func appendAsync(id: String, content: String, callback!: ?callbackfunc = None): Future<Index>

    /**
     * 异步添加数据
     * 
     * 参数 id - String,索引ID
     * 参数 content - String,索引内容
     * 参数 callback - ?callbackfunc,可参考具体回调函数类型
     * 
     * 返回值 Future<Index> 
     *
     * 异常 IllegalArgumentException：如果content为null(\0)字符，抛出异常
     */
    public func addAsync(id: String, content: String, callback!: ?callbackfunc = None): Future<Index>

    /**
     * 异步搜索数据
     * 
     * 参数 callback - ?callbacksearchfunc,可参考具体回调函数类型
     * 参数 querys - String，默认值为空
     * 参数 options - IndexSearchOptions，默认值为空构造
     * 
     * 返回值 Future<ArrayList<String>> 
     */
    public func searchAsync(callback!: ?callbacksearchfunc = None, querys!: String = "",
        options!: IndexSearchOptions = IndexSearchOptions()): Future<ArrayList<String>> 

    /**
     * 异步更新数据
     * 
     * 参数 id - String,索引ID
     * 参数 content - String,索引内容
     * 参数 callback - ?callbackfunc,可参考具体回调函数类型
     * 返回值 Future<Index> 
     */
    public func updateAsync(id: String, content: String, callback!: ?callbackfunc = None): Future<Index> 

    /**
     * 异步删除数据
     * 
     * 参数 id - String,索引ID
     * 参数 callback - ?callbackfunc,可参考具体回调函数类型
     * 返回值 Future<Index> 
     */
    public func removeAsync(id: String, callback!: ?callbackfunc = None): Future<Index>

    /**
     * 导入数据
     * 
     * 参数 key - String ,有四种导入选项，以cfg结尾的key值，导入optimize数据，以reg结尾的key值，导入fastupdate、register数据，以map结尾的key值，导入map数据，以ctx结尾的key值，导入ctx数据，
     * 参数 data - HashMap<String, Any> 导入数据存储的集合，其中键代表的是 id，唯一标识符，值代表的是 id 对应的存储数据。
     *
     */ 
    public func importIndex(key: String, data: HashMap<String, Any>): Unit

    /**
     * 数据导出
     * 
     * 参数 callback - CallbackExport= (id: String, data: HashMap<String, Any>) -> Unit
     * 返回值 Bool类型
     */
    public func exportIndex(callback: CallbackExport): Bool
    
}

// 异步回调函数，适用于 add、append、update、remove 方法
public type callbackfunc = (Index) -> Unit
// 异步回调函数，适用于 search 方法
public type callbacksearchfunc = (ArrayList<String>) -> Unit
```

#### 2.2 IndexSearchOptions
    数据搜索选项参数
```
public class IndexSearchOptions {

    /**
     * 数据查询范围，默认值为 100
     */
    public mut prop limit: Int64

    /**
     * 数据起始位置，默认值为 0
     */
    public mut prop offset: Int64

    /**
     * 数据建议，默认值为 false
     */
    public mut prop suggest: Bool
}   
```

#### 2.3 Preset 一系列预设的Index选项
用户应通过Preset获取一个Option模板对象，其包含一系列的预设选项，用户可以修改此对象的内容。然后传给Document/Index

```cangjie
public enum Preset {
    MEMORY | PERFORMANCE | MATCH | SCORE | DEFAULT
    /**
     * 获取预设模板的克隆对象
     **/
    public func getIndexOptions(): IndexOptions
    /**
     * 获取预设模板的克隆对象
     **/
    public func getDocumentOptions(): IndexOptionsForDocumentSearch
}
```

### 3 Document 多字段索引搜索

前置条件：NA
场景：
支持 Document 多字段搜索索引搜索
约束：cangjie 语言传入的 data 数据使用 JsonValue 格式的值进行上下文传递，可作用于导入或导出
性能： NA
可靠性： NA


#### 3.1 Document 多字段搜索索引搜索类

```cangjie
public class Document {

    /**
     * Document 的有参构造方法
     * @param options - 构造 document 对象时传入的可选项内容
     */
    public init(options: IndexOptionsForDocumentSearch)

    /**
     * 将文档内容添加到 document 对象中
     * @param content - 待添加的文档内容
     * @param id - 文档标识符，用于唯一地标识文档。id 为命名参数，若不传，则默认 id 为 None；若传入实际 id 值，则将此 id 值作为唯一标识符
     *
     * @return 返回添加了文档内容后的文档对象
     */
    public func add(content: Content, id!: ?String = None): Document

    /**
     * 将文档内容追加到 document 对象中
     * @param content - 待追加的文档内容
     * @param id - 文档标识符，用于唯一地标识文档。id 为命名参数，若不传，则默认 id 为 None；若传入实际 id 值，则将此 id 值作为唯一标识符
     *
     * @return 返回追加了文档内容后的文档对象
     */
    public func append(content: Content, id!: ?String = None): Document

    /**
     * 将文档内容更新到 document 对象中
     * @param content - 待更新的文档内容
     * @param id - 文档标识符，用于唯一地标识文档。id 为命名参数，若不传，则默认 id 为 None；若传入实际 id 值，则将此 id 值作为唯一标识符
     *
     * @return 返回更新了文档内容后的文档对象
     */
    public func update(content: Content, id!: ?String = None): Document

    /**
     * 根据 id 标识符删除 document 对象中的特定文档内容
     * @param id - 文档标识符
     *
     * @return 返回删除特定文档内容后的文档对象
     */
    public func remove(id: String): Document

    /**
     * 在 document 文档中执行搜索操作
     * @param query - 传入要搜索的查询字符串或查询对象。它定义了搜索的条件和模式，query 是命名参数，若不传，则默认为 "" 字符串，查全局；若传入实际值，则按照实际条件或模式来进行搜索
     * @param options - 传入 document 搜索索引可选项，它定义了根据什么选项来搜索文档和最终要返回什么内容
     * 
     * @return ArrayList<ArrayList<String>> 返回查询结果，一个二维的数组集合
     */
    public func search(query!: String = "", options!: SearchOptions = SearchOptions()): ArrayList<ArrayList<String>>

    /**
     * 将文档内容添加到 document 对象中的异步方法
     * @param content - 待添加的文档内容
     * @param callback - 异步回调函数，命名参数。若不传，则默认为 None，则不进行回调；若传入实际回调函数，则执行回调函数
     * @param id - 文档标识符，用于唯一地标识文档。id 为命名参数，若不传，则默认 id 为 None；若传入实际 id 值，则将此 id 值作为唯一标识符
     * @return Future<Document>
     */
    public func addAsync(content: Content, callback!: ?CallbackFunc = None, id!: ?String = None): Future<Document>

    /**
     * 将文档内容追加到 document 对象中的异步方法
     * @param content - 待追加的文档内容
     * @param callback - 异步回调函数，命名参数。若不传，则默认为 None，则不进行回调；若传入实际回调函数，则执行回调函数
     * @param id - 文档标识符，用于唯一地标识文档。id 为命名参数，若不传，则默认 id 为 None；若传入实际 id 值，则将此 id 值作为唯一标识符
     * @return Future<Document>
     */
    public func appendAsync(content: Content, callback!: ?CallbackFunc = None, id!: ?String = None): Future<Document>

    /**
     * 将文档内容更新到 document 对象中的异步方法
     * @param content - 待更新的文档内容
     * @param callback - 异步回调函数，命名参数。若不传，则默认为 None，则不进行回调；若传入实际回调函数，则执行回调函数
     * @param id - 文档标识符，用于唯一地标识文档。id 为命名参数，若不传，则默认 id 为 None；若传入实际 id 值，则将此 id 值作为唯一标识符
     * @return Future<Document>
     */
    public func updateAsync(content: Content, callback!: ?CallbackFunc = None, id!: ?String = None): Future<Document>

    /**
     * 根据 id 删除 document 中的特定文档内容的异步方法
     * @param id - 文档标识符，用于唯一地标识文档
     * @param callback - 异步回调函数，命名参数。若不传，则默认为 None，则不进行回调；若传入实际回调函数，则执行回调函数
     * @return Future<Document>
     */
    public func removeAsync(id: String, callback!: ?CallbackFunc = None): Future<Document>

    /**
     * 在 document 文档中执行搜索操作的异步方法
     * @param callback - 异步回调函数
     * @param query - 传入要搜索的查询字符串或查询对象。它定义了搜索的条件和模式，query 是命名参数，若不传，则默认为 "" 字符串，查全局；若传入实际值，则按照实际条件或模式来进行搜索
     * @param options - 传入 document 搜索索引可选项，它定义了根据什么选项来搜索文档和最终要返回什么内容
     * @return Future<ArrayList<ArrayList<String>>>
     */
    public func searchAsync(query!: String = "", callback!: ?CallbackSearchFunc = None, options!: SearchOptions = SearchOptions()): Future<ArrayList<ArrayList<String>>>

    /**
     * 用于 document 文档导出功能
     * @param callback - 回调函数，CallbackExport=(id: String, data: HashMap<String, Any>) -> Unit
     *
     */
    public func exportDocument(callback: CallbackExport): Unit

    /**
     * 用于 document 文档导入功能
     * @param key - String 用于指定要导入的数据类型或字段，使用 document 导入功能时候，key 的参数必须以 "xxx(索引对象的名字)" + "." + "导入选项" 格式为准，例如："title.reg"，先导入 index 的四个导入选项数据，再导入 document 的导入选项，例如 tag 或 store 。 index 和 document 中的 reg 事实上丰富的是 register 这个属性，是全局的，当有多个 index 索引时，只需设置一次 reg 导入选项即可，若重复设置，抛出 NoneValueException: Value does not exist! 异常。
     * @param data - HashMap<String, Any> 用于把回调中的数据导入到某个 document 对象中，其中键代表的是某个具体的 index 对象的 id 唯一标识符，值代表的是 id 所对应的数据
     *
     */
    public func importDocument(id: String, data: HashMap<String, Any>): Unit

}
// 待追加的文档内容
public type Content = JsonValue
// 异步回调函数，适用于 add、append、update、remove 方法
public type CallbackFunc = (Document) -> Unit
// 异步回调函数，适用于 search 方法
public type CallbackSearchFunc = (ArrayList<ArrayList<String>>) -> Unit
// 异步导出方法，适用于 index 和 document 的 export 方法
public type CallbackExport = (id: String, data: HashMap<String, Any>) -> Unit

```

#### 3.2 IndexOptionsForDocumentSearch
```cangjie
public class IndexOptionsForDocumentSearch <: IndexOptions {
    public mut prop document: ?DocumentOptions
}
```

#### 3.2 DocumentOptions document 初始化可选项

```cangjie
public class DocumentOptions {

    // content 中作为 id 的字段，是唯一的标识符，默认值为字符串 id
    public mut prop id: String

    // content中作为 tag 的标签字段，默认值为 tag[] 代表 JsonArray 数组
    public mut prop tag: String

    // content 中作为索引的字段，每个字段使用上下文默认的IndexOptions，默认值为 None
    public mut prop index: ?Array<String>

    // content 中作为数据存储的字段，当需要显示详情时，输出结果会含有 store 中存储的内容，默认值为 None
    public mut prop store: ?Array<String>

    // content 中的一个特定字段或域，默认值为 None
    public mut prop field: ?Array<String>
}
```

#### 3.4 SearchOptions document 搜索可选项，继承了 index 的搜索可选项

```cangjie
public class SearchOptions <: IndexSearchOptions {

    // 用于从查询结果中提取特定字段的方法，输出结果中可能包含多个字段，可以提取其中的任意一个或多个，默认值为 None
    public mut prop pluck: ?ArrayList<String>

    // 存储了 index 索引对象的键值，也就是标识符的数组集合，默认值为 None
    public mut prop index: ?ArrayList<String>

    // 存储了一个或多个特定字段或域，默认值为 None
    public mut prop field: ?ArrayList<String>

    // 存储了一个或多个标签字段，默认值为 None
    public mut prop tag: ?Array<String>

    // 表示输出结果中是否需要详情结果，默认值为 false
    public mut prop enrich: Bool

    // 用于组合多个查询条件的方法，允许创建一个复杂的查询，其中包含多个子查询，默认值为 "or"
    public mut prop bool: String
}
```

