## 原flex库
nodejs支持两种异步api：Promise/WebWorker
flexsearch.Index 支持同步和异步(Promise)两种API
flexsearch.WorkerIndex 使用 WebWorker 包装了Index的API

## ohos flex库
flexsearch.Index 支持同步和异步(Promise)两种API
flexsearch.WorkerIndex 使用 ArkWorker 包装了Index的API

## cangjie
cangjie只有一种异步API：Thread
只需实现 flexsearch.Index 的同步/异步API 即可
无需实现 flexsearch.WorkerIndex