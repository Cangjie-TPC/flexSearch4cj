/*
 * @Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

package flexsearch
import std.collection.*
public type CharsetEncode = (String) -> ArrayList<String>
public type CharsetRtl = Bool
public type CharsetTokenize = String

public open class CharsetOptions {
    public static let Default: CharsetOptions = CharsetOptions(CharsetLatinDefault.encode, CharsetLatinDefault.rtl, CharsetLatinDefault.tokenize)
    public static let Simple: CharsetOptions = CharsetOptions(CharsetLatinSimple.encode, CharsetLatinSimple.rtl, CharsetLatinSimple.tokenize)
    public static let Balance: CharsetOptions = CharsetOptions(CharsetLatinBalance.encode, CharsetLatinBalance.rtl, CharsetLatinBalance.tokenize)
    public static let Advanced: CharsetOptions = CharsetOptions(CharsetLatinAdvanced.encode, CharsetLatinAdvanced.rtl,
        CharsetLatinAdvanced.tokenize)
    public static let Extra: CharsetOptions = CharsetOptions(CharsetLatinExtra.encode, CharsetLatinExtra.rtl, CharsetLatinExtra.tokenize)
    public static let CjkDefault: CharsetOptions = CharsetOptions(CharsetCjkDefault.encode, CharsetCjkDefault.rtl, CharsetCjkDefault.tokenize)
    public static let CyrDefault: CharsetOptions = CharsetOptions(CharsetCyrillicDefault.encode, CharsetCyrillicDefault.rtl,
        CharsetCyrillicDefault.tokenize)
    public static let AraDefault: CharsetOptions = CharsetOptions(CharsetArabicDefault.encode, CharsetArabicDefault.rtl,
        CharsetArabicDefault.tokenize)

    let _encode: CharsetEncode
    let _rtl: CharsetRtl
    let _tokenize: CharsetTokenize

    /*
     * three parameter construction of CharsetOptions
     * @param encode - type of CharsetEncode
     * @param rtl - type of CharsetRtl
     * @param tokenize - type of CharsetTokenize
    */
    public init(encode: CharsetEncode, rtl: CharsetRtl, tokenize: CharsetTokenize) {
        this._encode = encode
        this._rtl = rtl
        this._tokenize = tokenize
    }

    public mut prop encode: CharsetEncode {
        get() {
            _encode
        }
        set(_) {}
    }
    public mut prop rtl: CharsetRtl {
        get() {
            _rtl
        }
        set(_) {}
    }
    public mut prop tokenize: CharsetTokenize {
        get() {
            _tokenize
        }
        set(_) {}
    }
}
