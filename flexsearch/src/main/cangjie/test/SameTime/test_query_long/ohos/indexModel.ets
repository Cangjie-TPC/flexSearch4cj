/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import FlexSearch, { Id, IndexOptions } from '@ohos/flexsearch';
import langEN from '../utils/langEN'
import suite from "./commonValues";
import text_data  from "./data";

for (let i = 0; i < text_data.length; i++) {
  suite.add(i, text_data[i]);
}
FlexSearch.registerLanguage('en', langEN);
const options: IndexOptions<string> = {
  preset: 'default',
  tokenize: 'strict',
  cache: true,
  resolution: 9,
  context: false,
  optimize: true,
  boost: undefined,
  charset: 'latin',
  language: 'en',
  encode: false,
  stemmer: false,
  filter: false,
  matcher: false,
};
const index = new FlexSearch.Index(options);

function checkId(id: Id) {
}

function addIndex(id: Id, content: string) {
}

function asyncAddIndex(id: Id, content: string) {
}

function appendIndex(id: Id, content: string) {
}

function asyncAppendIndex(id: Id, content: string) {
}

function updateIndex(id: Id, content: string) {
}

function asyncUpdateIndex(id: Id, content: string) {
}


function deleteIndex(id: Id) {
}

function asyncDeleteIndex(id: Id) {
}

function search(text: string) {
  runBench_queryLong_01(100,10000)
  runBench_queryLong_01(300,10000)
  runBench_queryLong_01(500,10000)
  runBench_queryLong_01(100,20000)
  runBench_queryLong_01(500,20000)
}

function asyncSearch(query: string) {
}

class ExportedData {
}

let exportedData: ExportedData[] = [];

async function exportIndex() {
}

function importIndex() {
}

export class FunctionDataType {
  name: string;
  fun: Function;

  constructor(name: string, fun: Function) {
    this.name = name;
    this.fun = fun;
  }
}

const model: FunctionDataType[] = [
  new FunctionDataType('addIndex', addIndex),
  new FunctionDataType('deleteIndex', deleteIndex),
  new FunctionDataType('updateIndex', updateIndex),
  new FunctionDataType('appendIndex', appendIndex),
  new FunctionDataType('exportIndex', exportIndex),
  new FunctionDataType('importIndex', importIndex),
  new FunctionDataType('search', search)
]

export default model;

async function task_async_multy(loopCount: number): Promise<string[]>{
  const result: Array<string> = Array<string>();
  for(let j = 0; j < loopCount; j++) {
    const re_tmp = suite.search("I cannot help it").toString();
    if (re_tmp == "17") {
      result.push("success")
    } else {
      result.push("fail")
    }
  }
  return result
}

async function task_async_long(loopCount: number): Promise<string[]>{
  const result: Array<string> = Array<string>();
  for(let j = 0; j < loopCount; j++) {
    const re_tmp = suite.search("I had now been two years in this country; and about the beginning of the third, Glumdalclitch and I attended the King and Queen in a progress to the coast of the kingdom").toString();
    if (re_tmp == "1235") {
      result.push("success")
    } else {
      result.push("fail")
    }
  }
  return result
}

async function task_async_dupe(loopCount: number): Promise<string[]>{
  const result: Array<string> = Array<string>();
  for(let j = 0; j < loopCount; j++) {
    const re_tmp = suite.search("I made the Captain a very low bow, and then turning to the Dutchman, said, I was sorry to find more mercy in a heathen, than in a brother Christian").toString();
    if (re_tmp == "1378") {
      result.push("success")
    } else {
      result.push("fail")
    }
  }
  return result
}

function runBench_queryMulty_01(threadCount: number, loopCount: number) {
  const promiseArr: Array<Promise<Array<string>>>=[];
  let start = new Date().getTime();
  for (let i = 0; i < threadCount; i++) {
    promiseArr.push(task_async_multy(loopCount));
  }
  let end = new Date().getTime();
  let s = (end - start) / 1000;
  let sum = threadCount * loopCount;
  console.log("test_queryMulty_01_" + threadCount + "*" + loopCount + " TPS: " + sum/s);
}

function runBench_queryLong_01(threadCount: number, loopCount: number) {
  const promiseArr: Array<Promise<Array<string>>>=[];
  let start = new Date().getTime();
  for (let i = 0; i < threadCount; i++) {
    promiseArr.push(task_async_long(loopCount));
  }
  let end = new Date().getTime();
  let s = (end - start) / 1000;
  let sum = threadCount * loopCount;
  console.log("test_queryLong_01_" + threadCount + "*" + loopCount + " TPS: " + sum/s);
}

function runBench_queryDupe_01(threadCount: number, loopCount: number) {
  const promiseArr: Array<Promise<Array<string>>>=[];
  let start = new Date().getTime();
  for (let i = 0; i < threadCount; i++) {
    promiseArr.push(task_async_dupe(loopCount));
  }
  let end = new Date().getTime();
  let s = (end - start) / 1000;
  let sum = threadCount * loopCount;
  console.log("test_queryDupe_01_" + threadCount + "*" + loopCount + " TPS: " + sum/s);
}