// EXEC: cjc %import-path %L %l %f
// EXEC: ./main

from flexSearch4cj import flexSearch4cj.*
from std import unittest.*
from std import unittest.testmacro.*
from std import collection.*
from encoding import json.*

main() {
    var doc = DocumentTestAppend()
    doc.test01()
    doc.test02()
    doc.test03()
    doc.test04()
    doc.test05()
    doc.test06()
    doc.test07()
    doc.test08()
    doc.test09()
    doc.test10()
    return 0
}

@Test
public class DocumentTestAppend {
    var jv: JsonValue = JsonValue.fromStr(
      ##"{
        "id":"id0",
        "tag":["tag1","tag2","tag3"],
        "str1":"val1",
        "arr2":["v21","v22","v23"],
        "arr3":[
          {"k3":"v31","k311":"v311"},
          {"k3":"v32"},
          {"k3":"v33"}
        ],
        "obj4":{
          "k41":"v41",
          "k42":"v42",
          "obj43":{
            "obj431":"v431",
            "obj432":"v432",
            "obj433":"v433"
          }
        }
      }"##)

    var jvAppend: JsonValue = JsonValue.fromStr(
      ##"{
        "tag":["tag4"],
        "str1":"val2",
        "arr2":["v223"]
      }"##)

    @TestCase
    public func test01(): Unit {
        var flag = 0
        var documentOption = DocumentOptions()
        documentOption.id = "id"
        documentOption.tag = "tag[]"
        documentOption.index = ["str1","arr2[]"]
        documentOption.store = ["arr3[]:k3","obj4:obj43:obj433"]
        var search = IndexOptionsForDocumentSearch()
        search.document = documentOption
        var d: Document = Document(search)
        try {
          d.add(jv)
          d.append(jvAppend, id: "id")
        } catch(e: Exception) {
          flag = 1
        }
        @Assert(flag, 0)
    }

    @TestCase
    public func test02(): Unit {
        var flag = 0
        var documentOption = DocumentOptions()
        var search = IndexOptionsForDocumentSearch()
        search.document = documentOption
        var d: Document = Document(search)
        try {
          d.add(jv)
          d.append(jvAppend, id: "id3")
        } catch(e: Exception) {
          flag = 1
        }
        @Assert(flag, 0)
    }

    @TestCase
    public func test03(): Unit {
        var flag = 0
        var documentOption = DocumentOptions()
        documentOption.id = "str1"
        documentOption.tag = "tag[]"
        documentOption.index = ["str1","arr2[]"]
        documentOption.store = ["obj4:obj43:obj433"]
        var search = IndexOptionsForDocumentSearch()
        search.document = documentOption
        var d: Document = Document(search)
        try {
          d.add(jv)
          d.append(jvAppend, id: "id")
        } catch(e: Exception) {
          flag = 1
        }
        @Assert(flag, 0)
    }

    @TestCase
    public func test04(): Unit {
        var flag = 0
        var documentOption = DocumentOptions()
        documentOption.tag = ""
        documentOption.index = ["str2","arr4[]"]
        documentOption.store = ["obj4:obj43:obj432"]
        var search = IndexOptionsForDocumentSearch()
        search.document = documentOption
        var d: Document = Document(search)
        try {
          d.add(jv)
          d.append(jvAppend, id: "id11")
        } catch(e: Exception) {
          flag = 1
        }
        @Assert(flag, 0)
    }

    @TestCase
    public func test05(): Unit {
        var flag = 0
        var documentOption = DocumentOptions()
        documentOption.tag = "obj4"
        documentOption.index = ["str2","arr4[]"]
        documentOption.store = ["obj4:obj43:obj432"]
        var search = IndexOptionsForDocumentSearch()
        search.document = documentOption
        var d: Document = Document(search)
        try {
          d.add(jv)
          d.append(jvAppend, id: "id")
        } catch (e: JsonException) {
          flag = 1
          println("document 的 tag 可选项在设置时无法作为 JsonObject 对象的 key 存储...")
        }
        @Assert(flag, 1)
    }

    @TestCase
    public func test06(): Unit {
        var flag = 0
        var documentOption = DocumentOptions()
        documentOption.tag = "str1"
        documentOption.index = ["str2","arr4[]"]
        documentOption.store = ["obj4:obj43:obj432"]
        var search = IndexOptionsForDocumentSearch()
        search.document = documentOption
        var d: Document = Document(search)
        try {
          d.add(jv)
          d.append(jvAppend, id: "id")
        } catch(e: Exception) {
          flag = 1
        }
        @Assert(flag, 0)
    }

    @TestCase
    public func test07(): Unit {
        var flag = 0
        var documentOption = DocumentOptions()
        documentOption.tag = "str1"
        documentOption.index = ["str2","arr4[]"]
        documentOption.store = ["obj4:obj43:obj432"]
        documentOption.field = ["str2", "arr3[]"]
        var search = IndexOptionsForDocumentSearch()
        search.document = documentOption
        var d: Document = Document(search)
        try {
          d.append(jvAppend)
        } catch(e: Exception) {
          flag = 1
        }
        @Assert(flag, 0)
    }

    @TestCase
    public func test08(): Unit {
        var flag = 0
        var documentOption = DocumentOptions()
        documentOption.id = "str1"
        documentOption.tag = "str1"
        documentOption.index = ["str2","arr4[]"]
        documentOption.store = ["obj4:obj43:obj432"]
        documentOption.field = ["str2", "arr2[]"]
        var search = IndexOptionsForDocumentSearch()
        search.document = documentOption
        var d: Document = Document(search)
        try {
          d.append(jvAppend)
        } catch(e: Exception) {
          flag = 1
        }
        @Assert(flag, 0)
    }

    @TestCase
    public func test09(): Unit {
        var flag = 0
        var documentOption = DocumentOptions()
        documentOption.field = ["str2", "arr4[]"]
        var search = IndexOptionsForDocumentSearch()
        search.document = documentOption
        var d: Document = Document(search)
        try {
          d.append(jvAppend)
        } catch(e: Exception) {
          flag = 1
        }
        @Assert(flag, 0)
    }

    @TestCase
    public func test10(): Unit {
        var flag = 0
        var documentOption = DocumentOptions()
        documentOption.tag = "obj43"
        var search = IndexOptionsForDocumentSearch()
        search.document = documentOption
        var d: Document = Document(search)
        try {
          d.append(jvAppend)
        } catch (e: JsonException) {
          flag = 1
        }
        @Assert(flag, 0)
    }
}
