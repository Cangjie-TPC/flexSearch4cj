/*
 * @Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

package flexsearch

import std.collection.*
import encoding.json.*

class Common {

    protected static func parseOption(value: ?Bool, default_value: Bool): Bool {
        if (let Some(v) <- value) {
            return v
        } else {
            return default_value
        }
    }

    protected static func createObjectArray(count: Int64): ArrayList<JsonObject> {
        var arr: ArrayList<JsonObject> = ArrayList<JsonObject>(count)
        for (i in 0..count) {
            arr.insert(i,createObject())
        }
        return arr
    }

    protected static func createArrays(count: Int64): ArrayList<ArrayList<JsonObject>> {
        var arr: ArrayList<ArrayList<JsonObject>> = ArrayList<ArrayList<JsonObject>>(count)
        for (i in 0..count) {
            arr.insert(i,ArrayList<JsonObject>())
        }
        return arr
    }

    protected static func createObject(): JsonObject {
        return JsonObject()
    }

    protected static func concat(arrays: ArrayList<ArrayList<String>>): ArrayList<String> {
        var length: Int64 = 0
        var pos: Int64 = 0
        for (a in arrays) {
            length += a.size
        }
        var result: ArrayList<String> = ArrayList<String>(length)
        for (a in arrays) {
            a.toArray().copyTo(result.toArray(), 0, pos, a.size)
            pos += a.size
        }
        return result
    }

    protected static func sortByLengthDown(a: ArrayList<String>, b: ArrayList<String>): Int64 {
        return b.size - a.size
    }
}
