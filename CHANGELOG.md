# 1.0.1
- 升级仓颉版本 0.53.18

## 1.0.0

- 实现了 Index\document\workerIndex 的一些依赖方法
  - 约束：上下文传递的数据是 Json 类型的值或对象，对标 js 库的 Object

- 实现了 Index 单字段搜索索引的增删改查方法
- 实现了 Document 文档搜索索引的增删改查方法
- 同步实现了 Index Document 的增删改查的异步方法
