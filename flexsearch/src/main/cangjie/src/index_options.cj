/*
 * @Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

package flexsearch

import encoding.json.*
import std.collection.*

public open class IndexOptions {
    var _cache: CacheClass = CacheClass(-1)
    public mut prop cache: CacheClass {
        get() {
            _cache
        }
        set(v) {
            this._cache = v
        }
    }

    var _rtl: Bool = false
    public mut prop rtl: Bool {
        get() {
            _rtl
        }
        set(v) {
            _rtl = v
        }
    }

    var _encode: EncodeFunc = CharsetLatinDefault.encode
    public mut prop encode: EncodeFunc {
        get() {
            _encode
        }
        set(v) {
            _encode = v
        }
    }

    var _tokenize: String = "strict"
    public mut prop tokenize: String {
        get() {
            _tokenize
        }
        set(v) {
            _tokenize = v
        }
    }

    var _fastupdate: Bool = true
    public mut prop fastupdate: Bool {
        get() {
            _fastupdate
        }
        set(v) {
            this._fastupdate = v
        }
    }

    var _minlength: Int64 = 1
    public mut prop minlength: Int64 {
        get() {
            _minlength
        }
        set(v) {
            this._minlength = v
        }
    }

    var _optimize: Bool = true
    public mut prop optimize: Bool {
        get() {
            _optimize
        }
        set(v) {
            this._optimize = v
        }
    }

    var _resolution: Int64 = 9
    public mut prop resolution: Int64 {
        get() {
            _resolution
        }
        set(v) {
            this._resolution = v
        }
    }

    var _contextDepth: Int64 = 1
    public mut prop contextDepth: Int64 {
        get() {
            _contextDepth
        }
        set(v) {
            this._contextDepth = v
        }
    }

    var _contextResolution: Int64 = 1
    public mut prop contextResolution: Int64 {
        get() {
            _contextResolution
        }
        set(v) {
            this._contextResolution = v
        }
    }

    var _contextBidirectional: Bool = true
    public mut prop contextBidirectional: Bool {
        get() {
            _contextBidirectional
        }
        set(v) {
            this._contextBidirectional = v
        }
    }

    var _boost: ?BoostFunc = None
    public mut prop boost: ?BoostFunc {
        get() {
            _boost
        }
        set(v) {
            this._boost = v
        }
    }

    var _matcher: ?HashMap<String, String> = None
    public mut prop matcher: ?HashMap<String, String> {
        get() {
            _matcher
        }
        set(v) {
            _matcher = v
        }
    }

    var _stemmer: ?HashMap<String, String> = None
    public mut prop stemmer: ?HashMap<String, String> {
        get() {
            _stemmer
        }
        set(v) {
            _stemmer = v
        }
    }

    var _filter: ?ArrayList<String> = None
    public mut prop filter: ?ArrayList<String> {
        get() {
            _filter
        }
        set(v) {
            _filter = v
        }
    }
}

let memoryOption: IndexOptionsForDocumentSearch = {
     =>
    let opt = IndexOptionsForDocumentSearch()
    opt.resolution = 3
    opt.minlength = 4
    opt.fastupdate = false
    opt
}()
let performanceOption: IndexOptionsForDocumentSearch = {
     =>
    let opt = IndexOptionsForDocumentSearch()
    opt.resolution = 3
    opt.minlength = 3
    opt.optimize = false
    opt.contextDepth = 2
    opt.contextResolution = 1
    opt
}()
let matchOption: IndexOptionsForDocumentSearch = {
     =>
    let opt = IndexOptionsForDocumentSearch()
    opt.tokenize = "reverse"
    opt
}()
let scoreOption: IndexOptionsForDocumentSearch = {
     =>
    let opt = IndexOptionsForDocumentSearch()
    opt.resolution = 20
    opt.minlength = 3
    opt.contextDepth = 3
    opt.contextResolution = 9
    opt
}()
let defaultOption: IndexOptionsForDocumentSearch = {
     => IndexOptionsForDocumentSearch()
}()

public enum Preset {
    MEMORY | PERFORMANCE | MATCH | SCORE | DEFAULT

    /**
    * Gets a clone of the default indexOptions template
    */
    public func getIndexOptions(): IndexOptions {
        match (this) {
            case MEMORY => (memoryOption.clone() as IndexOptions).getOrThrow()
            case PERFORMANCE => (performanceOption.clone() as IndexOptions).getOrThrow()
            case MATCH => (matchOption.clone() as IndexOptions).getOrThrow()
            case SCORE => (scoreOption.clone() as IndexOptions).getOrThrow()
            case DEFAULT => (defaultOption.clone() as IndexOptions).getOrThrow()
        }
    }

    /**
    * Gets a clone of the default documentOptions template
    */
    public func getDocumentOptions(): IndexOptionsForDocumentSearch {
        match (this) {
            case MEMORY => memoryOption.clone()
            case PERFORMANCE => performanceOption.clone()
            case MATCH => matchOption.clone()
            case SCORE => scoreOption.clone()
            case DEFAULT => defaultOption.clone()
        }
    }
}

public class IndexOptionsForDocumentSearch <: IndexOptions {
    var _document: ?DocumentOptions = None
    public mut prop document: ?DocumentOptions {
        get() {
            _document
        }
        set(v) {
            this._document = v
        }
    }

    func clone(): IndexOptionsForDocumentSearch {
        let opt = IndexOptionsForDocumentSearch()
        opt.fastupdate = fastupdate
        opt.minlength = minlength
        opt.optimize = optimize
        opt.resolution = resolution

        // charset
        opt.rtl = rtl
        opt.encode = encode
        opt.tokenize = tokenize

        // context
        opt.contextDepth = contextDepth
        opt.contextResolution = contextResolution
        opt.contextBidirectional = contextBidirectional

        // language
        opt.stemmer = stemmer
        opt.matcher = matcher
        opt.filter = filter

        opt
    }
}

public type EncodeFunc = (String) -> ArrayList<String>
public type BoostFunc = (ArrayList<String>, String, Int64) -> Float64
