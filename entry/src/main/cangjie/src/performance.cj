/*
 * @Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */
package ohos_app_cangjie_entry
import ohos.base.LengthProp
import ohos.component.Column
import ohos.component.Button
import ohos.component.Text
import ohos.component.Flex
import ohos.component.FlexParams
import ohos.component.Scroller
import ohos.component.CustomView
import ohos.component.FlexDirection
import ohos.component.ItemAlign
import ohos.component.FlexAlign
import ohos.component.Alignment
import ohos.component.Row
import ohos.component.ScrollDirection
import ohos.component.loadNativeView
import ohos.component.CJEntry
import ohos.component.Scroll
import ohos.state_macro_manage.Entry
import ohos.state_macro_manage.Component
import ohos.state_macro_manage.State
import ohos.state_manage.ObservedProperty
import ohos.state_manage.SubscriberManager
import flexsearch.*
@Entry
@Component
class PerformancePage {
    var scroller: Scroller = Scroller()
    @State var hint_init: String = "请完成初始化"
    @State var test_hint: String = "等待测试"
    var tfi: TestFuncIndex = TestFuncIndex()
    var testIndex:Index = tfi.getNoneIndex()

    func build() {
        Flex(FlexParams(direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Start)) {
                Text(hint_init).fontSize(20).align(Alignment.Start).padding(10)
                Row() {
                    Button("初始化").width(80.percent).fontSize(20).onClick({ =>
                                          spawn {
                                                     hint_init="正在进行初始化，请稍后...."
                                                     testIndex=tfi.IndexInit()
                                                     hint_init="初始化完成"
                                                }
                                     })
                }
                Text("").padding(10)
                Text(test_hint).fontSize(20).align(Alignment.Start).padding(10)
                Scroll(this.scroller){
                    Column(){
                         Button("test_queryDupes_01").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_queryDupes_01(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                             })
                        Button("test_queryDupes_02").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_queryDupes_02(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                            })

                        Button("test_queryLong_01").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_queryLong_01(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                            })
                        Button("test_queryLong_02").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_queryLong_02(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                           })
                        Button("test_queryLong_03").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_queryLong_03(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_queryMulti_01").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_queryMulti_01(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_queryMulti_02").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_queryMulti_02(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_queryMulti_03").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_queryMulti_03(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_queryMulti_04").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_queryMulti_04(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_queryMulti_05").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_queryMulti_05(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_queryNotFound_01").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_queryNotFound_01(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_queryNotFound_02").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_queryNotFound_02(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_queryNotFound_03").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_queryNotFound_03(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_querySingle_01").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_querySingle_01(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_querySingle_02").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_querySingle_02(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_querySingle_03").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_querySingle_03(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_querySingle_04").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_querySingle_04(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_querySingle_05").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_querySingle_05(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_querySingle_06").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_querySingle_06(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_querySingle_07").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_querySingle_07(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_querySingle_08").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_querySingle_08(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_querySingle_09").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_querySingle_09(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                        Button("test_querySingle_10").width(80.percent).fontSize(20).onClick({ =>
                                                                  spawn {
                                                                             hint_init="开始执行"
                                                                             tfi.test_querySingle_10(testIndex)
                                                                             hint_init="执行结束"
                                                                        }
                                                             })
                    }
                 }.scrollable(ScrollDirection.Vertical)

        }
        .width(100.percent)
        .height(100.percent)
    }
}
