<div align="center">
<h1>flexSearch4cj</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v1.0.1-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/build-pass-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.53.18-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-NA-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/project-open-brightgreen" style="display: inline-block;" />
</p>

## 介绍

FlexSearch 是一个快速、零依赖的全文搜索库。 在原始搜索速度方面，FlexSearch 优于每一个搜索库， 并提供灵活的搜索功能，如多字段搜索，语音转换或部分匹配。根据使用的选项，它还提供最高内存效率的索引。 FlexSearch 引入了一种新的评分算法，称为“上下文索引”，基于预先评分的词典字典体系结构，与其他库相比，实际执行的查询速度有大幅度提高。 FlexSearch 还为您提供非阻塞异步处理模型，以通过专用平衡线程并行地对索引执行任何更新或查询。

### 特性

- 🚀 支持多种语系编码场景
- 🚀 支持配置多种初始化选项
- 🚀 支持 Index 索引、Document 多字段索引搜索

### 源码目录

```shell
├── AppScope
├── doc
├── entry0
│   └── src
│       └── main 
│           ├── cangjie
│           │   └── src
│           └──resources
├── flexsearch
│   └── src
│       └── main
│           ├── cangjie
│           │   └── src
│           └── resource
├── hvigor
├── CHANGELOG.md
├── LICENSE
├── README.md
├── README.OpenSource
```

- `AppScope` 全局资源存放目录和应用全局信息配置目录
- `doc` API文档和使用手册存放目录
- `entry` 工程模块 - 编译生成一个HAP
- `entry src` APP代码目录
- `entry src main` APP项目目录
- `entry src main cangjie` 仓颉UI页面目录
- `entry src main resources` 资源文件目录
- `flexsearch` 工程模块 - 编译生成一个har包
- `flexsearch src` 模块代码目录
- `flexsearch src main` 模块项目目录
- `flexsearch src main cangjie` 仓颉代码目录
- `flexsearch src main resources` 资源文件目录
- `flexsearch src main cangjie src` 仓颉源码目录
- `hvigor` 构建工具目录

### 接口说明

主要是核心类和成员函数说明,详情见 [API](./doc/feature_api.md)

## 使用说明

### 编译构建

1. 通过module引入
    1. 克隆下载项目
    2. 将 flexsearch 模块拷贝到应用项目下
    3. 修改自身应用 entry 下的 oh-package.json5 文件，在 dependencies 字段添加 "flexsearch": "file:../flexsearch"
       ```json
       {
          "name": "entry",
          "version": "1.0.0",
          "description": "Please describe the basic information.",
          "main": "",
          "author": "",
          "license": "",
          "dependencies": {
            "flexsearch": "file:../flexsearch"
          }
        }
       ```
    4. 修改自身应用 entry/src/main/cangjie 下的 cjpm.toml 文件，在 [dependencies] 字段下添加 flexsearch = {path = "../../../../flexsearch/src/main/cangjie", version = "1.0.0"}
       ```toml
       [dependencies]
           flexsearch = {path = "../../../../flexsearch/src/main/cangjie", version = "1.0.0"}
       ```
    5. 在项目中使用 import flexsearch.* 引用flexsearch项目
       ```cangjie
       import flexsearch.*
       ```

### 功能示例

#### document 多字段索引

```cangjie
import flexsearch.*

// 初始化
var documentOptions = DocumentOptions()
var document = Document(documentOptions)
// 添加
var target=JsonObject(HashMap<String,JsonValue>([("url",JsonString("url15")),
                                               ("tag",JsonString("拼接")),
                                               ("title",JsonString("拼接前")),
                                               ("content",JsonString("这是拼接前的值"))
                                              ]))
document.add(target,id:Option<String>.Some("appendTest"))
// 设置查询选项,进行查询 
var searchOption=SearchOptions()
//设置查询tag
searchOption.tag=Option<Array<String>>.Some(Array<String>(["房产","疫情"])) 
//设置查询显示条数
searchOption.limit=3
//在指定列查询
searchOption.index=ArrayList<String>(["title"])
//设置查询结果偏移量
searchOption.offset=0
document.search(query:"房地产", options:searchOption)
// 查询回调函数,根据需求自定义
public func CallbackSearchFunc(index:ArrayList<ArrayList<String>>):Unit{
    index.append(ArrayList<String>(["CallbackSearchFunc"]))
}
// 异步查询
document.searchAsync(query:"房地产",callback:CallbackSearchFunc)
// 操作回调函数，根据需求自定义
public func CallbackFunc (document:Document):Unit{
    var target=JsonObject(HashMap<String,JsonValue>([("url",JsonString("callback01")),
                                                   ("tag",JsonString("callback01")),
                                                   ("title",JsonString("callback01")),
                                                   ("content",JsonString("callback01"))
                                                  ]))
    document.add(target)
}
// 追加                                            
document.append(target,id:"appendTest")
// 异步追加                                            
document.appendAsync(target,id:"appendTest",callback:CallbackFunc)
// 更新
document.update(target,id:"updateTest")
// 异步更新
document.updateAsync(target,callback:CallbackFunc,id:"updateTest")
// 移除
document.remove("updateTest")
// 异步移除
document.removeAsync("updateTest",callback:CallbackFunc)
//设置导出容器
var docdata=HashMap<String, HashMap<String, Any>>()
//设置导出回调函数
public func callbackExportDocument(id: String, pojo: HashMap<String, Any>) :Unit{
    docdata.put(id,pojo)
}
// 导出数据
document.exportDocument(callbackExportDocument)
// 从容器导入数据，其中v为导出数据的容器,k为导入数据的对应的选项文件
document.importDocument("title.reg", docdata)
document.importDocument("title.cfg", docdata)
document.importDocument("title.map", docdata)
document.importDocument("title.ctx", docdata)
document.importDocument("title.oos", docdata)
```


#### Index 索引

```cangjie
import flexsearch.*

// 初始化 
var options = Preset.DEFAULT.getIndexOptions()
var index = Index(options)
// 添加
index.add("26","z zz zzz")
// 异步追加,可根据需求自定义回调函数
// 定义操作回调函数
public func callbackOperatorfunc(index:Index):Unit{
    index.add("1","callback")
}
index.addAsync("26", "z zz zzz",callback:callbackOperatorfunc)
// 查询
index.search(querys:"z zz zzz")
// 定义异步查询回调函数,可根据需求自定义
public func callbackSearchfunc(arr:ArrayList<String>):Unit{
    for (i in 0..arr.size){
        arr[i]+="end"
    }
}
// 异步查询
index.searchAsync(callback:callbackSearchfunc,querys:"appendIndex")
// 追加
index.append("52","appendIndex")
// 异步追加
index.appendAsync("52","appendIndex",callback:callbackOperatorfunc)
// 移除
index.remove("26")
// 异步移除
index01.removeAsync("26",callback:callbackOperatorfunc)
// 更新
index.update("52","updateIndex")
// 异步更新
index.updateAsync("26", "updateIndex",callback:callbackOperatorfunc)

// 定义导出容器
var data=HashMap<String, Any>()
// 定义导出回调，可根据需求自定义
public func callbackExportIndex(id: String, pojo: HashMap<String, Any>) :Unit{
    data.put(id,pojo[id])
}
// 导出
index.exportIndex(callbackExportIndex)
// 导入
index.importIndex("reg", data)
index.importIndex("cfg", data)
index.importIndex("map", data)
index.importIndex("ctx", data)
index.importIndex("oos", data)

```

## 约束与限制

在下述版本验证通过：

    DevEco Studio 5.0.3 Release(5.0.9.300)      
    Cangjie Support Plugin 5.0.9.100      
    Cangjie Compiler: 0.53.18 (cjnative)

## 开源协议

本项目基于 [Apache License 2.0](./LICENSE) ，请自由的享受和参与开源。

## 参与贡献

欢迎给我们提交 PR，欢迎给我们提交 issue，欢迎参与任何形式的贡献。
